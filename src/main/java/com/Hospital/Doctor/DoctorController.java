package com.Hospital.Doctor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class DoctorController {
	@Autowired
	private DoctorRepo docrepo;
	
	@PostMapping("doctor")
	public void doctorclass(@RequestBody DoctorPojo dc) {
		
		
		docrepo.save(dc);
		System.out.println(dc);
	}

}
