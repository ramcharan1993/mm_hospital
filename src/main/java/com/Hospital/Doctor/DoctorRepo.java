package com.Hospital.Doctor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository												
public interface DoctorRepo extends JpaRepository<DoctorPojo, Integer> {
											//	<pojo class name,primary key type>
}
